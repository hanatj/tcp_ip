/*
 * m2m_tcp_test.c
 *
 *  Created on: 09 apr 2018
 *      Author: AlessioQu
 */


#include <stdio.h>
#include <string.h>
#include <time.h>
#include "m2mb_types.h"
#include "m2mb_os_types.h"
#include "m2mb_os_api.h"
#include "m2mb_os.h"
#include "m2mb_fs_posix.h"
#include "m2mb_net.h"
#include "m2mb_rtc.h"  // for get timezone
#include "m2mb_pdp.h"
#include "m2mb_socket.h"
#include <sys/time.h>   // for gettimeofday()

#include "m2m_log.h"
#include "m2m_utils.h"

#include "m2m_tasks.h"
#include "m2m_tcp_test.h"


/* Macro =============================================================================*/
#define SERVER "modules.telit.com"
#define SERVER_PORT 10510

#define APN	    "internetm2m.air.com"
#define PDP_CTX 	(UINT8)3

/* Global variables =============================================================================*/

M2MB_OS_SEM_HANDLE RegLockHandle = NULL;
M2MB_OS_SEM_HANDLE pdpLockHandle = NULL;

M2MB_PDP_HANDLE pdpHandle;
M2MB_SOCKET_BSD_SOCKET sock_client = M2MB_SOCKET_BSD_INVALID_SOCKET;




/* Global functions =============================================================================*/
int get_host_ip_by_name(const CHAR* host)
{
	UINT32 r_addr = 0;
	struct M2MB_SOCKET_BSD_HOSTENT *phe;
	struct M2MB_SOCKET_BSD_HOSTENT he;
	char tmpbuf[1024];
	int i, herr;
	if ( ( ( i = m2mb_socket_bsd_get_host_by_name_2_r_cid( host, M2MB_SOCKET_BSD_AF_INET,  &he, tmpbuf, (SIZE_T) 1024, &phe, (INT32*) &herr, PDP_CTX) ) != 0 ) ||
	     ( phe == NULL ) )
	{
		return 0;
	}
	else
	{
		memcpy((char *)&r_addr, phe->h_addr_list[0], phe->h_length);
		return r_addr;
	}
}

INT32 get_pending_bytes(M2MB_SOCKET_BSD_SOCKET s, INT32 *pending)
{
	INT32 optlen=sizeof(int);
	int ret;
	ret = m2mb_socket_bsd_get_sock_opt( s, M2MB_SOCKET_BSD_SOL_SOCKET, M2MB_SOCKET_BSD_SO_RXDATA, (void*) pending, &optlen );
	if( 0 != ret)
	{
		return -1;
	}
	else
	{
		return 0;
	}

}


void NetCallback(M2MB_NET_HANDLE h, M2MB_NET_IND_E net_event, UINT16 resp_size, void *resp_struct, void *myUserdata)
{
	M2MB_NET_REG_STATUS_T *stat_info;

	switch (net_event)
	{

#if 0
	case M2MB_NET_REG_STATUS_IND:
		M2M_LOG_DEBUG("Network change event!\r\n");
		m2mb_net_get_reg_status_info(h);
		break;
#endif

    case M2MB_NET_GET_REG_STATUS_INFO_RESP:
      stat_info = (M2MB_NET_REG_STATUS_T*)resp_struct;
      if  (stat_info->stat == 1 || stat_info->stat == 5)
      {
    	  M2M_LOG_DEBUG("Module is registered to cell 0x%X!\r\n", stat_info->cellID);
    	  m2mb_os_sem_put(RegLockHandle);
      }
      else if (stat_info->stat == 2)
	  {
    	  m2mb_net_get_reg_status_info(h); //call it again
	  }
      break;


	default:
		M2M_LOG_DEBUG("unexpected net_event: %d\r\n", net_event);
		break;

	}
}

void PdpCallback(M2MB_PDP_HANDLE h, M2MB_PDP_IND_E pdp_event, UINT8 cid, void *userdata)
{
	struct M2MB_SOCKET_BSD_SOCKADDR_IN CBtmpAddress;
	M2MB_RESULT_E ret;
	CHAR CBtmpIPaddr[32];

	switch (pdp_event)
	{
		case M2MB_PDP_UP:
			M2M_LOG_DEBUG ("Context activated!\r\n");
			ret = m2mb_pdp_get_my_ip(h, cid, M2MB_PDP_IPV4, &CBtmpAddress.sin_addr.s_addr);
			m2mb_socket_bsd_inet_ntop( M2MB_SOCKET_BSD_AF_INET, &CBtmpAddress.sin_addr.s_addr, ( CHAR * )&( CBtmpIPaddr ), sizeof( CBtmpIPaddr ) );
			M2M_LOG_DEBUG( "IP address: %s\r\n", CBtmpIPaddr);
			m2mb_os_sem_put(pdpLockHandle);
			break;

		case M2MB_PDP_DOWN:
			M2M_LOG_DEBUG ("Context successfully deactivated!\r\n");
			break;
		default:
			M2M_LOG_DEBUG("unexpected pdp_event: %d\r\n", pdp_event);
			break;

	}
}

INT32 M2M_msgTCPTask(INT32 type, INT32 param1, INT32 param2)
{
    M2MB_RESULT_E retVal = M2MB_RESULT_SUCCESS;
    time_t stTime_s = 0;
    time_t stTime_m = 0;
    time_t stTime_h = 0;
    time_t enTime_s = 0;
    time_t enTime_m = 0;
    time_t enTime_h = 0;
    time_t stVal_s = 0;
    time_t stVal_ms = 0;
    time_t enVal_s = 0;
    time_t enVal_ms = 0;


    M2MB_NET_HANDLE h;
    M2MB_OS_SEM_ATTR_HANDLE semAttrHandle;
    struct M2MB_SOCKET_BSD_TIMEVAL timeOutVal;

    INT32 timeOutLen = sizeof(struct M2MB_SOCKET_BSD_TIMEVAL);
    struct M2MB_SOCKET_BSD_SOCKADDR_IN stSockAddr;
    char ip_addr[64];
    char send_buf[64];
    char recv_buf[64];
    int ret;
    int task_status;
    int i;
    static int	toSend;

    CHAR apn[32], apnUser[16], apnPwd[16];

    INT32 res = M2MB_SOCKET_BSD_SOCKNOERROR;
    void *myUserdata;
    void *argp;

    do
    {
		M2M_LOG_DEBUG("INIT\r\n");

		if (RegLockHandle == NULL)
		{
			m2mb_os_sem_setAttrItem( &semAttrHandle, CMDS_ARGS( M2MB_OS_SEM_SEL_CMD_CREATE_ATTR,  NULL,M2MB_OS_SEM_SEL_CMD_COUNT, 0 /*IPC*/, M2MB_OS_SEM_SEL_CMD_TYPE, M2MB_OS_SEM_GEN,M2MB_OS_SEM_SEL_CMD_NAME, "regSem"));
			m2mb_os_sem_init( &RegLockHandle, &semAttrHandle );
		}
		if (pdpLockHandle == NULL)
		{
			m2mb_os_sem_setAttrItem( &semAttrHandle, CMDS_ARGS( M2MB_OS_SEM_SEL_CMD_CREATE_ATTR,  NULL,M2MB_OS_SEM_SEL_CMD_COUNT, 0 /*IPC*/, M2MB_OS_SEM_SEL_CMD_TYPE, M2MB_OS_SEM_GEN,M2MB_OS_SEM_SEL_CMD_NAME, "pdpSem"));
			m2mb_os_sem_init( &pdpLockHandle, &semAttrHandle );
		}

		m2mb_fs_unlink((const CHAR *) "/core/file_load");

		retVal = m2mb_net_init(&h, NetCallback, myUserdata);
		if ( retVal == M2MB_RESULT_SUCCESS )
		{
			M2M_LOG_DEBUG( "m2mb_net_init returned M2MB_RESULT_SUCCESS\r\n");
		}
		else
		{
			M2M_LOG_ERROR( "m2mb_net_init not returned M2MB_RESULT_SUCCESS\r\n" );
		}


			M2M_LOG_DEBUG("Waiting for registration...\r\n");

		retVal = m2mb_net_get_reg_status_info(h);
		if ( retVal != M2MB_RESULT_SUCCESS )
		{
			M2M_LOG_ERROR( "m2mb_net_get_reg_status_info did not return M2MB_RESULT_SUCCESS\r\n" );
		}
		m2mb_os_sem_get(RegLockHandle, M2MB_OS_WAIT_FOREVER );


		M2M_LOG_DEBUG("Pdp context activation\r\n");
		retVal = m2mb_pdp_init(&pdpHandle, PdpCallback, myUserdata);
		if ( retVal == M2MB_RESULT_SUCCESS )
		{
			M2M_LOG_DEBUG( "m2mb_pdp_init returned M2MB_RESULT_SUCCESS\r\n");
		}
		else
		{
			M2M_LOG_DEBUG( "m2mb_pdp_init did not return M2MB_RESULT_SUCCESS\r\n" );
		}

		sleep_ms(2000);

		memset( apn, 0x00, sizeof(apn));
		memset( apnUser, 0x00, sizeof(apnUser) );
		memset( apnPwd, 0x00, sizeof(apnPwd) );

		strcat( apn, APN );

		M2M_LOG_DEBUG("Activate PDP with APN %s....\r\n", apn);
		retVal = m2mb_pdp_activate(pdpHandle, PDP_CTX, apn, apnUser, apnPwd, M2MB_PDP_IPV4); //activates cid 3 with APN "internet.wind.biz" and IP type IPV4
		if ( retVal != M2MB_RESULT_SUCCESS )
		{
			M2M_LOG_ERROR("cannot activate pdp context.\r\n");
		}
		m2mb_os_sem_get(pdpLockHandle, M2MB_OS_WAIT_FOREVER );

		M2M_LOG_DEBUG("Creating Socket...\r\n");

		sock_client = m2mb_socket_bsd_socket(M2MB_SOCKET_BSD_AF_INET, M2MB_SOCKET_BSD_SOCK_STREAM, M2MB_SOCKET_BSD_IPPROTO_TCP);
		if (M2MB_SOCKET_BSD_INVALID_SOCKET == sock_client)
		{
			M2M_LOG_DEBUG("TCP Client Error\r\n");
			return 1;

		}
		else
		{
			M2M_LOG_DEBUG("Socket created\r\n");
		}


		if ( m2mb_socket_set_cid( sock_client, PDP_CTX ) != 0 )
		{
			M2M_LOG_ERROR( "Socket not set to ctx: %d\r\n", PDP_CTX );
			return -1;
		}

		M2M_LOG_DEBUG( "Socket ctx set to %d\r\n", PDP_CTX );

		memset(&stSockAddr, 0, sizeof(struct M2MB_SOCKET_BSD_SOCKADDR_IN));

		if (0 == (stSockAddr.sin_addr.s_addr= get_host_ip_by_name(SERVER)))
		{
			M2M_LOG_ERROR("Cannot retrieve IP\r\n");
			task_status = APPLICATION_EXIT;
			break;
		}
		else
		{
			m2mb_socket_bsd_inet_ntop(M2MB_SOCKET_BSD_AF_INET, (const void*) &(stSockAddr.sin_addr.s_addr), ip_addr, sizeof(ip_addr));
			M2M_LOG_DEBUG("Retrieved IP: %s \r\n", ip_addr);
		}

		stSockAddr.sin_family = M2MB_SOCKET_BSD_PF_INET;
		stSockAddr.sin_port = m2mb_socket_bsd_htons(SERVER_PORT);

		ret = m2mb_socket_bsd_ioctl( sock_client, M2MB_SOCKET_BSD_FIONREAD, argp );
		if (ret ==-1)
		{
			M2M_LOG_ERROR("CANNOT CHANGE IO ..\r\n");
			break;
		} else {
			M2M_LOG_DEBUG(" IO CHANGED..\r\n");
		}
		if( 0 != m2mb_socket_bsd_connect(sock_client, (struct M2MB_SOCKET_BSD_SOCKADDR*)&stSockAddr,
															sizeof(struct M2MB_SOCKET_BSD_SOCKADDR_IN)))
		{
			M2M_LOG_ERROR("Cannot connect socket!\r\n");
			task_status = APPLICATION_EXIT;
			break;

		}

		M2M_LOG_DEBUG("Socket Connected!\r\n");

		memset(send_buf, 0, sizeof(send_buf));
		strcpy(send_buf, "hello from m2mb Telit ME910!");

		toSend = strlen(send_buf);
//    	comupte starting time
		M2M_LOG_DEBUG("Sending data over socket..\r\n");


		INT32 fd = m2mb_rtc_open("/dev/rtc0", 0);


        if (fd >= 0) {
        	M2MB_RTC_TIME_T time;
	        M2MB_RTC_TIMEVAL_T timeval;

            m2mb_rtc_ioctl(fd, M2MB_RTC_IOCTL_GET_SYSTEM_TIME, &time);
	        m2mb_rtc_ioctl(fd, M2MB_RTC_IOCTL_GET_TIMEVAL, &timeval);

            stTime_m= time.min;
            stTime_s = time.sec;
            stTime_h= time.hour;
            stVal_s= timeval.sec;
            stVal_ms= timeval.msec;
            m2mb_rtc_close(fd);
        }
        M2M_LOG_DEBUG("Starting Time: (%d:%d:%d)  .\r\n", stTime_h,stTime_m,stTime_s);

		ret = m2mb_socket_bsd_send (sock_client, send_buf, toSend,0);

		if (ret ==-1)
		{
			M2M_LOG_ERROR("CANNOT SEND DATA OVER SOCKET, Error: %d\r\n", m2mb_socket_errno());
			task_status = APPLICATION_EXIT;
			break;
		}
		else
		{
			if (ret == toSend)
			{
				M2M_LOG_DEBUG("Data send successfully (%d bytes) .\r\n", toSend);
			}
			else
			{
				M2M_LOG_ERROR("Only %d bytes sent\r\n", ret);
				task_status = APPLICATION_EXIT;
				break;
			}
		}

//		sleep_ms(2000);
		memset(recv_buf, 0, sizeof(recv_buf));

		M2M_LOG_DEBUG("trying to receive %d bytes..\r\n", toSend);
		ret = m2mb_socket_bsd_recv(sock_client, recv_buf, toSend,0);
		if (ret ==-1)
		{
			M2M_LOG_ERROR("CANNOT RECEIVE DATA OVER SOCKET, errno: %d\r\n", m2mb_socket_errno());
		}
		else
		{
			M2M_LOG_DEBUG("Data received (%d): <%s>  .\r\n", ret, recv_buf);

			//    compute end time

		    INT32 fd2 = m2mb_rtc_open("/dev/rtc0", 0);


	        if (fd2 >= 0) {
	        	M2MB_RTC_TIME_T time;
				M2MB_RTC_TIMEVAL_T timeval;

	            m2mb_rtc_ioctl(fd2, M2MB_RTC_IOCTL_GET_SYSTEM_TIME, &time);
				m2mb_rtc_ioctl(fd2, M2MB_RTC_IOCTL_GET_TIMEVAL, &timeval);

	            enTime_m= time.min;
	            enTime_s = time.sec;
	            enTime_h= time.hour;
	            enVal_s= timeval.sec;
				enVal_ms= timeval.msec;
	            m2mb_rtc_close(fd2);
	        }
	        M2M_LOG_DEBUG("Ending Time: (%d:%d:%d)  .\r\n", enTime_h,enTime_m,enTime_s);

	        M2M_LOG_DEBUG("Execution Time Duration: (%d:%d:%d)  .\r\n", (enTime_h-stTime_h),(enTime_m-stTime_m),(enTime_s-stTime_s));
	        M2M_LOG_DEBUG("Execution Time Duration in ms: (%d:%d)  .\r\n", (enVal_s-stVal_s),(enVal_ms-stVal_ms));

		}
		task_status = APPLICATION_EXIT;
		break;

    } while (0);

    if (task_status == APPLICATION_EXIT)
    {
    	M2M_LOG_DEBUG("application exit\r\n");

		m2mb_socket_bsd_close( sock_client );

		ret = m2mb_pdp_deactivate(pdpHandle, PDP_CTX);
		if(ret != M2MB_RESULT_SUCCESS)
		{
			M2M_LOG_ERROR("CANNOT DEACTIVATE PDP\r\n");
			return -1;
		}
		else
		{
			M2M_LOG_DEBUG("m2mb_pdp_deactivate returned success \r\n");
		}

		M2M_LOG_DEBUG("Application complete.\r\n");
    }



}
