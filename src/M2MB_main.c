/* $version: 301813  */ 
/*===============================================================================================*/
/*         >>> Copyright (C) Telit Communications S.p.A. Italy All Rights Reserved. <<<          */
/*!
  @file
    M2M_main.c

  @brief
    The file contain the main user entry point of Appzone

  @details
    <Detailed description of the file>

  @notes
    Start of Appzone: Entry point
    User code entry is in function M2M_main()

  @author


  @date
    02/03/2017
*/
/* Include files ================================================================================*/
#include "m2mb_types.h"
#include "m2mb_os_types.h"
#include "m2mb_os_api.h"
#include "m2mb_os.h"
#include "m2mb_net.h"
#include "m2mb_info.h"

#include "m2m_log.h"
#include "m2m_utils.h"

#include "m2m_tasks.h"

#include "m2m_tcp_test.h"



/* Local defines ================================================================================*/
/* define to disable DEBUG_SIM */
/* #defined( NDEBUG_SIM ) */

/* Local typedefs ===============================================================================*/

/* Local statics ================================================================================*/
/* Local function prototypes ====================================================================*/
/* Static functions =============================================================================*/
/* Global functions =============================================================================*/

/*-----------------------------------------------------------------------------------------------*/

/* =================================================================================================

   DESCRIPTION:     Handles events sent to process 1

   PARAMETERS:      type: event id
                    param1: address to message context received, if any: 0 otherwise
                    param2: addition info

   RETURNS:         0 if message \ event is managed .

   PRE-CONDITIONS:  None.

   POST-CONDITIONS: None.

   IMPORTANT NOTES: Called from process 1 after event \ message is received
                    Used to manage received event \ message
   ============================================================================================== */


/***************************************************************************************************
   \User Entry Point of Appzone

   \param [in] Module Id

   \details Main of the appzone user
**************************************************************************************************/
void myCallback(M2MB_NET_HANDLE h, M2MB_NET_IND_E net_event, UINT16 resp_size, void *resp_struct, void *myUserdata)
{
  M2MB_NET_DESCRIPTION_T *listPnt;
  switch(net_event)
  {
	case M2MB_NET_GET_SIGNAL_INFO_RESP:
	{
	  M2MB_NET_GET_SIGNAL_INFO_RESP_T *resp = (M2MB_NET_GET_SIGNAL_INFO_RESP_T*)resp_struct;
	  M2M_LOG_DEBUG("GET Signal Info resp is %d, %d \r\n", resp->rat, resp->rssi);
	  if((resp->sigInfo != NULL) && (resp->rat == M2MB_NET_RAT_UTRAN) || ((resp->rat >= M2MB_NET_RAT_UTRAN_wHSDPA) && (resp->rat <= M2MB_NET_RAT_UTRAN_wHSDPAandHSUPA)))
	  {
		M2MB_NET_SIGNAL_INFO_UTRAN_T *tmpSigInfo = (M2MB_NET_SIGNAL_INFO_UTRAN_T*)(resp->sigInfo);
		M2M_LOG_DEBUG("GET Signal Info resp is %d \r\n", tmpSigInfo->ecio);
	  }
	  else if((resp->sigInfo != NULL) && (resp->rat == M2MB_NET_RAT_EUTRAN))
	  {
		M2MB_NET_SIGNAL_INFO_EUTRAN_T *tmpSigInfo = (M2MB_NET_SIGNAL_INFO_EUTRAN_T*)(resp->sigInfo);
		M2M_LOG_DEBUG("GET Signal Info resp is %d, %d, %d \r\n", tmpSigInfo->rsrq, tmpSigInfo->rsrp, tmpSigInfo->snr);
	  }
	  break;
	}
	case M2MB_NET_GET_BER_RESP:
	{
	  M2MB_NET_GET_BER_RESP_T *resp = (M2MB_NET_GET_BER_RESP_T*)resp_struct;
	  M2M_LOG_DEBUG("GET BER resp is %d, %d  .\r\n", resp->rat, resp->ber);
	  break;
	}

	case M2MB_NET_GET_CURRENT_OPERATOR_INFO_RESP:
	{
	  M2MB_NET_GET_CURRENT_OPERATOR_INFO_RESP_T *resp = (M2MB_NET_GET_CURRENT_OPERATOR_INFO_RESP_T*)resp_struct;
	  M2M_LOG_DEBUG("GET current operator resp is %d, %d .\r\n", resp->mcc, resp->mnc);
	  break;
	}

    case M2MB_NET_GET_AVAILABLE_NW_LIST_RESP:
    {
		 M2MB_NET_GET_AVAILABLE_NW_LIST_RESP_T *resp = (M2MB_NET_GET_AVAILABLE_NW_LIST_RESP_T*)resp_struct;
		 M2M_LOG_DEBUG("GET list length is %d .\r\n", resp->availableNetworks_size);
		 listPnt = resp->availableNetworks;
		 while(listPnt != NULL)
		 {
			 M2M_LOG_DEBUG("Available NW info are %d, %d, %d, %d .\r\n", listPnt->mcc, listPnt->mnc, listPnt->networkAv, listPnt->rat);
		   listPnt = listPnt->next;
		 }
   }
  }
}

void M2MB_main( int argc, char **argv )
{
	//user code
	INT32 res;
	INT8 taskID;
	M2M_LOG_CFG log_cfg;

	res = m2m_task_first_init();

	sleep_ms(5000);

	/*SET output channel */
	log_cfg.channel = _LOG_UART_MAIN; /* Select _LOG_UART_MAIN: MUST be used with AT#PORTCFG=8 */

	log_cfg.logMask = LOG_DEBUG_MASK;

	m2m_log_init(&log_cfg);  /* write code fulfilling the requirements of the M2M project */
	CHAR *data;
	M2MB_RESULT_E retVal = M2MB_RESULT_SUCCESS;
	M2MB_INFO_HANDLE h;
	retVal = m2mb_info_init(&h);


	//Print all levels with default macros
	M2M_LOG_INFO("Starting. This is build: %s %s. MASK: %04X\r\n", __DATE__, __TIME__, m2m_log_get_log_mask());

	taskID = m2m_create_task((char*) "TCP_TASK", M2M_TASKS_STACK_L, 1, M2M_TASKS_MBOX_M, M2M_msgTCPTask);

	M2M_LOG_DEBUG_MORE("Task ID: %d.\r\n", taskID);
	retVal = m2mb_info_get(h, M2MB_INFO_GET_SERIAL_NUM, &data);
	if ( retVal == M2MB_RESULT_SUCCESS )
	{
		M2M_LOG_DEBUG( "Device IMSI is %s.\r\n", data);
	}

	M2MB_RESULT_E retVall = M2MB_RESULT_SUCCESS;
	M2MB_NET_HANDLE hand;
	M2MB_NET_GET_SIGNAL_INFO_RESP_T *myUserdata;
	retVall = m2mb_net_init(&hand, myCallback, (M2MB_NET_GET_SIGNAL_INFO_RESP_T*) myUserdata);

//
	retVall = m2mb_net_get_signal_info(hand);
	if ( retVall == M2MB_RESULT_SUCCESS ){
		M2M_LOG_DEBUG( "m2mb_net_get_signal_info request succeeded.\r\n");
	}


//     information about bit error rate.
    retVall = m2mb_net_get_ber(hand);
	if ( retVall == M2MB_RESULT_SUCCESS ){
		M2M_LOG_DEBUG( "m2mb_net_get_ber request succeeded.\r\n");
	}



//	 gets information about current registered network operator
	retVall = m2mb_net_get_current_operator_info(hand);
	 if ( retVall == M2MB_RESULT_SUCCESS ){
		 M2M_LOG_DEBUG( "m2mb_net_get_current_operator_info request succeeded.\r\n");
	}

 //	gets a list containing current availble networks.

	retVall = m2mb_net_init(&h, myCallback, (M2MB_NET_GET_AVAILABLE_NW_LIST_RESP_T* )myUserdata);
	retVall = m2mb_net_get_available_nw_list(hand);
	 if ( retVall == M2MB_RESULT_SUCCESS ){
		M2M_LOG_DEBUG( "m2mb_net_get_available_nw_list request succeeded.\r\n");
	 }

	if (taskID > 0)
	{
		m2mb_os_send_message_to_task( taskID, INIT, 0, 0);
	}
	else
	{
		M2M_LOG_ERROR("cannot create task!\r\n");
//		return;
	}

	sleep_ms(2000);

}

