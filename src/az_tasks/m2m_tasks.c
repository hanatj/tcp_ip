/*
 * m2m_tasks.c
 *
 *  Created on: 06 apr 2018
 *      Author: AlessioQu
 */

#include <stdio.h>
#include <string.h>

#include "m2mb_types.h"

#include "m2mb_os_types.h"
#include "m2mb_os_api.h"
#include "m2mb_os.h"


#include "m2m_utils.h"

#include "m2m_tasks.h"

//#include "m2m_log.h"
/* Global variables =============================================================================*/
_S_TASK_PARAMS m2mb_tasks;

#ifdef HDR_M2M_LOG_H_
#define TASKS_DEBUG 1
#else
#define TASKS_DEBUG 0
#endif


INT32 m2m_task_first_init()
{
	INT32 ret;
	int i;
	for(i = 0; i < _T_MAX_TASKS; i++ )
	{
		m2mb_tasks.task_slots[i].Task_H = M2MB_OS_TASK_INVALID;
		m2mb_tasks.task_slots[i].Task_NameBuf = NULL;
		m2mb_tasks.task_slots[i].SlotInUse = 0;
	}

	m2mb_tasks.M2MMain_Handle = m2mb_os_taskGetId(); //store
	m2mb_tasks.isInit = 1;

	/*
	* OS functions initialization
	*/
	//ret = m2mb_os_init();

	ret = M2MB_OS_SUCCESS;

	return ret;
}

static INT8 find_free_task_slot(void)
{
	int i;

	if (! m2mb_tasks.isInit)
	{
		return -1;
	}

	for (i=0; i< _T_MAX_TASKS; i++)
	{
		if(m2mb_tasks.task_slots[i].SlotInUse == 0)
		{
			m2mb_tasks.task_slots[i].SlotInUse = 1;
			return i;
		}
	}
	return -1;
}

static INT8 find_slot_by_handle(M2MB_OS_TASK_HANDLE h)
{
	int i;
	for (i=0; i< _T_MAX_TASKS; i++)
	{
		if(m2mb_tasks.task_slots[i].SlotInUse == 1)
		{
#if TASKS_DEBUG
		M2M_LOG_DEBUG_MORE("comparing task handle %p with current slot %d handle %p\r\n", h, i, m2mb_tasks.task_slots[i].Task_H );
#endif
			if (h == m2mb_tasks.task_slots[i].Task_H)
			{
				return i;
			}
		}
	}
	return -1;

}

//char* name must be big enough to contain task names.
char * m2mb_get_task_name(char *name)
{
	M2MB_OS_RESULT_E res;
	MEM_W out;
	M2MB_OS_TASK_HANDLE taskHandle = m2mb_os_taskGetId();
	if (taskHandle == m2mb_tasks.M2MMain_Handle)
	{
		strcpy(name, "M2M_MAIN");
	}
	else
	{
		if (M2MB_OS_SUCCESS != m2mb_os_taskGetItem( taskHandle, M2MB_OS_TASK_SEL_CMD_NAME, &out, NULL ))
		{
			return NULL;
		}
		else
		{
			strcpy(name, (char*)out);
		}
	}
	return name;
}

INT32 get_current_task_id(void)
{
	M2MB_OS_TASK_HANDLE h = m2mb_os_taskGetId();
	return find_slot_by_handle(h) + 1;
}


INT32 m2mb_os_send_message_to_task( INT8 TaskProcID, INT32 type, INT32 param1, INT32 param2 )
{
	TASK_MESSAGE tmpMsg;
	M2MB_OS_RESULT_E osRes;
	M2MB_OS_Q_HANDLE Queue_H;

	INT8 slot = TaskProcID - 1;

	if (slot < 0 || slot > _T_MAX_TASKS  -1 )
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR("task id %d not valid!\r\n", TaskProcID);
#endif
		return M2M_TASK_INVALID_ID_ERR;
	}

	if(m2mb_tasks.task_slots[slot].SlotInUse == 0)
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR("task id %d not existing\r\n", TaskProcID);
#endif
		return M2M_TASK_ID_NOT_DEFINED_ERR;
	}

	Queue_H = m2mb_tasks.task_slots[slot].Task_Queue_H;


	tmpMsg.type = type;
	tmpMsg.param1 = param1;
	tmpMsg.param2 = param2;

#if TASKS_DEBUG
	M2M_LOG_DEBUG_MORE( "message ==> type=%d; par1=%d; par2=%d\r\n", tmpMsg.type, tmpMsg.param1, tmpMsg.param2 );
#endif
	osRes = m2mb_os_q_tx( Queue_H, (void*)&tmpMsg, M2MB_OS_NO_WAIT, M2MB_OS_Q_TX_PRIORITIZE );

	if( osRes != M2MB_OS_SUCCESS )
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR( "Send message to Task %d failed; error %d\r\n", TaskProcID, osRes );
#endif
		return M2M_TASK_MSG_SEND_ERR;  // failure
	}
	else
	{
#if TASKS_DEBUG
		M2M_LOG_DEBUG_MORE("Message sent.\r\n");
#endif
		return M2M_TASK_OK;  // success
	}
}


/*
 *  Task --> EntryFn
 */
void Task_EntryFn( void *arg )
{
	M2MB_OS_TASK_HANDLE taskHandle = m2mb_os_taskGetId();

	INT32 slot = (INT32)arg;
	MEM_W  task_name = 0;

	if (slot == -1)
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR("Cannot find slot for handle %d\r\n", taskHandle);
#endif
		return;
	}

	//M2M_LOG_DEBUG("slot: %d\r\n", slot);



	TASK_MESSAGE inPars;

	m2mb_os_taskGetItem( taskHandle, M2MB_OS_TASK_SEL_CMD_NAME, &task_name, NULL );

#if TASKS_DEBUG
	M2M_LOG_DEBUG_MORE( "\r\n==>Task %s started!\r\n", (char*) task_name);
#endif

	while( 1 )
	{
		 m2mb_os_q_rx( m2mb_tasks.task_slots[slot].Task_Queue_H, (void*)&inPars, M2MB_OS_WAIT_FOREVER );

#if TASKS_DEBUG
		 M2M_LOG_DEBUG_MORE( "%s received a message: \r\n"
						 "- type   = %d\r\n"
						 "- param1 = %d\r\n"
						 "- param2 = %d\r\n\r\n",
						 (char*) task_name, inPars.type, inPars.param1, inPars.param2 );
#endif

		 m2mb_tasks.task_slots[slot].Task_UserCB( inPars.type, inPars.param1, inPars.param2 );
	 }

#if TASKS_DEBUG
	M2M_LOG_DEBUG_MORE("exiting entry function. \r\n");
#endif

}


INT32 m2m_create_task( char *task_name, INT32 stack_size, INT32 priority, INT32 msg_q_size, USER_TASK_CB cb)
{
	M2MB_OS_RESULT_E os_res;

	M2MB_OS_TASK_ATTR_HANDLE Task_Attr_H;
	M2MB_OS_Q_ATTR_HANDLE Task_Queue_Attr_H;

	INT32 task_prio;
	UINT32 queue_area_size;
	INT8 slot;



	if (! m2mb_tasks.isInit)
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR("m2m task first init not performed yet\r\n");
#endif
		return M2M_TASK_NOTINIT_ERR;
	}

	if (priority < 1 || priority > 32)
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR("priority out of bounds\r\n");
#endif
		return M2M_TASK_WRONG_PRIO_ERR;
	}
	else
	{
		task_prio = priority + 200; //201 to 232
	}

	if (stack_size < MIN_STACK_SIZE || stack_size > MAX_STACK_SIZE)  //1 to 32KB
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR("stack size out of bounds\r\n");
#endif
		return M2M_TASK_STACK_SIZE_ERR;
	}

	if (msg_q_size < MIN_Q_SIZE || msg_q_size > MAX_Q_SIZE)
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR("message queue size out of bounds\r\n");
#endif
		return M2M_TASK_MSG_Q_SIZE_ERR;
	}


	queue_area_size = msg_q_size * _T_QUEUE_MSG_SIZE *  sizeof( INT32); //50 * 3* sizeof( INT32)
	queue_area_size = msg_q_size * _T_QUEUE_MSG_SIZE * 4; //50 * 3* sizeof( INT32)

	//input parameters are valid, now get a free slot.
	slot = find_free_task_slot();

	if (slot == -1)
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR("No free slots!\r\n");
#endif
		return M2M_TASK_NO_FREE_SLOTS_ERR;
	}

	m2mb_tasks.task_slots[slot].Task_NameBuf = (char*) m2mb_os_malloc(_T_TASK_NAME_SIZE * sizeof(char));

	if(!m2mb_tasks.task_slots[slot].Task_NameBuf)
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR("cannot allocate name buffer\r\n");
#endif
		return M2M_TASK_ALLOC_ERR;
	}

	memset(m2mb_tasks.task_slots[slot].Task_NameBuf,0, _T_TASK_NAME_SIZE);

	if (task_name)
	{
		strncpy(m2mb_tasks.task_slots[slot].Task_NameBuf, task_name, _T_TASK_NAME_SIZE - 1);
	}
	else
	{
		sprintf(m2mb_tasks.task_slots[slot].Task_NameBuf, "Task%d", slot + 1);
	}

#if TASKS_DEBUG
	M2M_LOG_DEBUG_MORE("task_name_buf: %s\r\n", m2mb_tasks.task_slots[slot].Task_NameBuf);
#endif

	m2mb_tasks.task_slots[slot].Task_QueueArea = (UINT8*) m2mb_os_malloc(sizeof(UINT8) * queue_area_size);
	if(!m2mb_tasks.task_slots[slot].Task_QueueArea )
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR("Cannot allocate queuearea\r\n");
#endif

		m2mb_os_free(m2mb_tasks.task_slots[slot].Task_NameBuf);
		m2mb_tasks.task_slots[slot].SlotInUse = 0;
		return M2M_TASK_ALLOC_ERR;
	}
	memset(m2mb_tasks.task_slots[slot].Task_QueueArea, 0, queue_area_size);

	m2mb_tasks.task_slots[slot].Task_UserCB = cb;

#if TASKS_DEBUG
	M2M_LOG_DEBUG_MORE( "Create task messages queue\r\n" );
#endif
	if ( m2mb_os_q_setAttrItem( &Task_Queue_Attr_H, 1,M2MB_OS_Q_SEL_CMD_CREATE_ATTR,NULL) != M2MB_OS_SUCCESS )
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR( "SET QUEUE ATTRIBUTES - FAIL  --> SKIP QUEUE CREATION !!!\r\n" );
#endif
		m2mb_os_free(m2mb_tasks.task_slots[slot].Task_QueueArea);
		m2mb_os_free(m2mb_tasks.task_slots[slot].Task_NameBuf);
		m2mb_tasks.task_slots[slot].SlotInUse = 0;
		return M2M_TASK_Q_ATTRIB_SET_ERR;
	}
	else
	{
		os_res = m2mb_os_q_setAttrItem( &Task_Queue_Attr_H, 4,
								   M2MB_OS_Q_SEL_CMD_NAME, M2MB_OS_Q_SEL_CMD_QSTART,
								   M2MB_OS_Q_SEL_CMD_MSG_SIZE, M2MB_OS_Q_SEL_CMD_QSIZE,
								   "Myqueue1", &(m2mb_tasks.task_slots[slot].Task_QueueArea), _T_QUEUE_MSG_SIZE, queue_area_size
								 );
		if ( M2MB_OS_SUCCESS != os_res )
		{
#if TASKS_DEBUG
			M2M_LOG_ERROR( "SET QUEUE ATTRIBUTES - FAIL\r\n" );
#endif
			m2mb_os_free(m2mb_tasks.task_slots[slot].Task_QueueArea);
			m2mb_os_free(m2mb_tasks.task_slots[slot].Task_NameBuf);
			m2mb_tasks.task_slots[slot].SlotInUse = 0;
			return M2M_TASK_Q_ATTRIB_SET_ERR;
		}
		else
		{
#if TASKS_DEBUG
			M2M_LOG_DEBUG_MORE( "Set queue attributes done\r\n" );
#endif
			//sleep_ms( 1000 );

			os_res = m2mb_os_q_init( &(m2mb_tasks.task_slots[slot].Task_Queue_H), &Task_Queue_Attr_H  );
			if ( M2MB_OS_SUCCESS != os_res )
			{
#if TASKS_DEBUG
				M2M_LOG_ERROR( "QUEUE CREATE - FAIL\r\n" );
#endif
				m2mb_os_free(m2mb_tasks.task_slots[slot].Task_QueueArea);
				m2mb_os_free(m2mb_tasks.task_slots[slot].Task_NameBuf);
				m2mb_tasks.task_slots[slot].SlotInUse = 0;
				return M2M_TASK_Q_INIT_ERR;
			}
			else
			{
				//M2M_LOG_DEBUG_MORE( "QUEUE CREATE - PASS\r\n" );
				os_res = m2mb_os_taskSetAttrItem( &Task_Attr_H,
												  CMDS_ARGS(
															 M2MB_OS_TASK_SEL_CMD_CREATE_ATTR, NULL,
															 M2MB_OS_TASK_SEL_CMD_STACK_SIZE, (void*)stack_size,
															 M2MB_OS_TASK_SEL_CMD_NAME, m2mb_tasks.task_slots[slot].Task_NameBuf,
															 M2MB_OS_TASK_SEL_CMD_PRIORITY, task_prio,      /* Use priority values above 200. max value: 255 (less priority) */
															 M2MB_OS_TASK_SEL_CMD_PREEMPTIONTH, task_prio,
															 M2MB_OS_TASK_SEL_CMD_AUTOSTART, M2MB_OS_TASK_AUTOSTART,
															 M2MB_OS_TASK_SEL_CMD_USRNAME, m2mb_tasks.task_slots[slot].Task_NameBuf
														   )
												);
				if ( M2MB_OS_SUCCESS != os_res )
				{
#if TASKS_DEBUG
					M2M_LOG_ERROR( "SET TASK ATTRIBUTES - FAIL\r\n" );
#endif
					m2mb_os_free(m2mb_tasks.task_slots[slot].Task_QueueArea);
					m2mb_os_free(m2mb_tasks.task_slots[slot].Task_NameBuf);
					m2mb_tasks.task_slots[slot].SlotInUse = 0;
					return M2M_TASK_ATTRIB_SET_ERR;
				}
				/*
				else
				{
					M2M_LOG_DEBUG_MORE( "SET TASK ATTRIBUTES - PASS\r\n" );
				}
				*/

#if TASKS_DEBUG
				M2M_LOG_DEBUG_MORE( "Creating the task\r\n" );
#endif
				os_res = m2mb_os_taskCreate(
								  &(m2mb_tasks.task_slots[slot].Task_H),
								  &Task_Attr_H,
								  &Task_EntryFn,
								  (void*)( (INT32) slot )
							);

				if ( M2MB_OS_SUCCESS != os_res )
				{
#if TASKS_DEBUG
					M2M_LOG_ERROR( "TASK CREATION FAIL\r\n" );
#endif
					m2mb_os_free(m2mb_tasks.task_slots[slot].Task_QueueArea);
					m2mb_os_free(m2mb_tasks.task_slots[slot].Task_NameBuf);
					m2mb_tasks.task_slots[slot].SlotInUse = 0;
					return M2M_TASK_CREATE_ERR;
				}
				else
				{
					if ( M2MB_OS_TASK_INVALID == m2mb_tasks.task_slots[slot].Task_H )
					{
#if TASKS_DEBUG
						M2M_LOG_ERROR( "INVALID TASK HANDLE - FAIL\r\n" );
#endif
						m2mb_os_free(m2mb_tasks.task_slots[slot].Task_QueueArea);
						m2mb_os_free(m2mb_tasks.task_slots[slot].Task_NameBuf);
						m2mb_tasks.task_slots[slot].SlotInUse = 0;
						return -1;
					}
#if TASKS_DEBUG
					M2M_LOG_DEBUG_MORE("Task created successfully with handle %p\r\n", m2mb_tasks.task_slots[slot].Task_H);
#endif

					return slot + 1;
				}

			}
		}
	}

	return -1 ;
}


/*NOT AVAILABLE AT THE MOMENT*/

INT32 m2m_destroy_task(INT8 TaskProcID)
{
#if 0
	INT8 slot = TaskProcID - 1;
	M2MB_OS_RESULT_E res;
	MEM_W  out = 0;
	M2MB_OS_Q_HANDLE Queue_H;
	TASK_MESSAGE tmpMsg;

#if TASKS_DEBUG
	M2M_LOG_DEBUG("destroy...\r\n");
#endif
	if (! m2mb_tasks.isInit)
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR("m2m task first init not performed yet\r\n");
#endif
		return -1;
	}

	if (slot < 0 || slot > _T_MAX_TASKS  -1 )
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR("task id %d not valid!\r\n", TaskProcID);
#endif
		return -1;
	}

	if(m2mb_tasks.task_slots[slot].SlotInUse == 0)
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR("task id %d not existing\r\n", TaskProcID);
#endif
		return -1;
	}

	Queue_H = m2mb_tasks.task_slots[slot].Task_Queue_H;

	tmpMsg.type = 9;
	tmpMsg.param1 = 1;
	tmpMsg.param2 = 1;

	res = m2mb_os_q_tx( Queue_H, (void*)&tmpMsg, M2MB_OS_NO_WAIT, M2MB_OS_Q_TX_PRIORITIZE );

	if( res != M2MB_OS_SUCCESS )
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR( "Send message to Task %d failed; error %d\r\n", TaskProcID, res );
#endif
		return -1;  // failure
	}
	else
	{
#if TASKS_DEBUG
		M2M_LOG_DEBUG("Message sent.\r\n");
#endif
	}

	sleep_ms(2000);

#if TASKS_DEBUG
	M2M_LOG_DEBUG("Trying to terminate task...\r\n");
#endif
	res = m2mb_os_taskTerminate(m2mb_tasks.task_slots[slot].Task_H);
	if (res != M2MB_OS_SUCCESS)
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR("Cannot terminate task %d\r\n", TaskProcID);
#endif
		return res;
	}

	m2mb_os_taskGetItem( m2mb_tasks.task_slots[slot].Task_H, M2MB_OS_TASK_SEL_CMD_STATE, &out, NULL );
#if TASKS_DEBUG
	M2M_LOG_DEBUG ( "Task state: %d\r\n", out );
#endif

	sleep_ms(1000);

#if TASKS_DEBUG
	M2M_LOG_DEBUG("Trying to delete task with handle %d...\r\n", m2mb_tasks.task_slots[slot].Task_H);
#endif
	res = m2mb_os_taskDelete(m2mb_tasks.task_slots[slot].Task_H);
	if (res != M2MB_OS_SUCCESS)
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR("Cannot delete task %d\r\n", TaskProcID);
#endif
		return res;
	}

#if TASKS_DEBUG
	M2M_LOG_DEBUG("Trying to deinit task queue...\r\n");
#endif
	res = m2mb_os_q_deinit(m2mb_tasks.task_slots[slot].Task_Queue_H);
	if (res != M2MB_OS_SUCCESS)
	{
#if TASKS_DEBUG
		M2M_LOG_ERROR("Cannot deinit task %d queue \r\n", TaskProcID);
#endif
		return res;
	}

#if TASKS_DEBUG
	M2M_LOG_DEBUG("Trying to free task queuearea...\r\n");
#endif
	m2mb_os_free(m2mb_tasks.task_slots[slot].Task_QueueArea);

#if TASKS_DEBUG
	M2M_LOG_DEBUG("Trying to free task name buffer...\r\n");
#endif
	m2mb_os_free(m2mb_tasks.task_slots[slot].Task_NameBuf);



	m2mb_tasks.task_slots[slot].Task_H = M2MB_OS_TASK_INVALID;
	m2mb_tasks.task_slots[slot].SlotInUse = 0;
#endif
	return 0;
}
