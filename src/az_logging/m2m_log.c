/*
 * m2m_log.c
 *
 *  Created on: 13 gen 2017
 *      Author: FabioPi
 */


/*=================================================================
#Telit Extensions
#
#Copyright � 2016, Telit Communications S.p.A.
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
#Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
#Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in
#the documentation and/or other materials provided with the distribution.
#
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS``AS IS'' AND ANY EXPRESS OR IMPLIED
#WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
#PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
#HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
#NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#POSSIBILITY OF SUCH DAMAGE.
#
==============================================================*/


/*==================================================================================================
                            INCLUDE FILES
==================================================================================================*/



#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "m2mb_types.h"
#include "m2mb_os_types.h"
#include "m2mb_os_api.h"
#include "m2mb_os.h"

#include "m2mb_os_sem.h"
#include "m2mb_usb.h"
#include "m2mb_uart.h"

#include "m2m_log.h"

#define USB_CH_MAX 3

M2MB_OS_SEM_HANDLE USIF0SemHandle = NULL;
M2MB_OS_SEM_HANDLE USIF1SemHandle = NULL;
M2MB_OS_SEM_HANDLE USBSemHandle[USB_CH_MAX];

_M2M_LOG_STRUCT LogBaseGlobalStruct;

INT32 g_USBfd = -1;
INT32 g_UART_fd = -1;
INT32 g_AUX_fd = -1;

/* PUBLIC functions*/


void m2m_log_init(M2M_LOG_CFG *log_cfg)
{

	int i;
	for(i=0; i<USB_CH_MAX; i++)
	{
		USBSemHandle[i] = NULL;
	}

	LogBaseGlobalStruct.gLog_Mask 		= log_cfg->logMask;
	LogBaseGlobalStruct.gLog_Channel  	= log_cfg->channel;
	LogBaseGlobalStruct.isInit 			= 1;
}

INT32 m2m_log_deinit()
{
	INT32 result;
	if ( ! LogBaseGlobalStruct.isInit)
	{
		return NOT_INIT_ERROR;
	}

	switch(LogBaseGlobalStruct.gLog_Channel)
	{
	case _LOG_UART_MAIN:
		if(g_UART_fd != -1)
		{
			result = m2mb_uart_close(g_UART_fd);
			g_UART_fd = -1;
		}
		break;
	case _LOG_UART_AUX:
		if(g_AUX_fd != -1)
		{
			result = m2mb_uart_close(g_AUX_fd);
			g_AUX_fd = -1;
		}
		break;
	case _LOG_USB0:
		if(g_USBfd != -1)
		{
			result = m2mb_usb_close(g_USBfd);
			g_USBfd = -1;
		}
		break;
	case _LOG_USB1:
		if(g_USBfd != -1)
		{
			result = m2mb_usb_close(g_USBfd);
			g_USBfd = -1;
		}
		break;
	default:
		//TODO return some error
		break;
	}
	LogBaseGlobalStruct.isInit = 0;
	return result;
}

void m2m_log_set_log_mask(UINT32 log_mask)
{

	LogBaseGlobalStruct.gLog_Mask 		= log_mask;
}

UINT32 m2m_log_get_log_mask(void)
{
	if(LogBaseGlobalStruct.isInit == 1)
	{
		return LogBaseGlobalStruct.gLog_Mask;
	}
	else
	{
		return 0x0;
	}
}

UINT32 * m2m_log_get_log_mask_pointer(void)
{
	return &(LogBaseGlobalStruct.gLog_Mask);
}



/* INTERNAL functions*/

const char* _m2m_log_get_file_title(const char* path)
{
	const char* p = path;

	while (*p) {
		if (*p == '/' || *p == '\\') {
			return p + 1;
		}

		p++;
	}
	return path;
}


char *m2m_os_get_current_task_name(char *name)
{
	M2MB_OS_RESULT_E res;
	MEM_W out;
	M2MB_OS_TASK_HANDLE taskHandle = m2mb_os_taskGetId();

	if (M2MB_OS_SUCCESS != m2mb_os_taskGetItem( taskHandle, M2MB_OS_TASK_SEL_CMD_NAME, &out, NULL ))
	{
		return NULL;
	}
	else
	{
		strcpy(name, (char*)out);
		return name;
	}



}


//##################################################################################################################################
/**
 *  \brief Prints on the requested log channel (USB, UART, AUX)
 *
 *	\param [in] msg: 		message to be printed on output
 *	\param [in] par1: 		generic parameter, unused for now
 *  \return :	amount of printed bytes,  negative value in case of error
 *
 */
//##################################################################################################################################
INT32 _m2m_log_base_function(const char *msg, void *par1)
{
	INT32 result;


	if ( ! LogBaseGlobalStruct.isInit)
	{
		return NOT_INIT_ERROR;
	}

	switch(LogBaseGlobalStruct.gLog_Channel)
	{
	case _LOG_UART_MAIN:
		result = _m2m_log_print_to_UART(msg);
		break;
	case _LOG_UART_AUX:
		result = _m2m_log_print_to_AUX_UART(msg);
		break;
	case _LOG_USB0:
		result = _m2m_log_print_to_USB("/dev/USB0", msg);
		break;
	case _LOG_USB1:
		result = _m2m_log_print_to_USB("/dev/USB1", msg);
		break;
	default:
		//TODO return some error
		break;
	}

	return result;
}


//##################################################################################################################################
/**
 *  \brief Prints on the defined stream (UART or USB channel)
 *
 *	\param [in] level: 		Logging level. see M2M_LOG_LEVEL_E enum
 *	\param [in] function: 	source function name to add to the output if log is verbose
 *	\param [in] file: 		source file path to add to the output if log is verbose
 *	\param [in] line: 		source file line to add to the output if log is verbose
 *  \param [in] fmt : 		string format with parameters to print
 *  \param [in] ... : 		...
 *  \return the number of sent bytes. 0 if logging level is not enabled, negative in case of error
 *
 */
//##################################################################################################################################
INT32 _m2m_log_formatted ( M2M_LOG_LEVEL_E level, const char* function, const char* file, int line, const CHAR *fmt, ... )
{

	INT32	sent = 0;
	va_list arg;
	int 	bufSize = 2048;

	CHAR 	*buf = NULL;
	INT32   offset = 0;
	UINT32  now = _m2m_log_get_Uptime();
	CHAR taskName[128];

	/* If the selected log level is set */
	if ( M2M_IS_LOG_LEVEL_SET(m2m_log_get_log_mask(), level))
	{
		/*Alloc required size*/
		buf = (CHAR *) m2mb_os_malloc(sizeof(CHAR) * bufSize);
		if (!buf)
		{
			_m2m_log_base_function("CANNOT ALLOCATE LOG BUFFER\r\n", NULL);
			return BUF_ALLOC_ERROR;
		}

		memset(buf,0,bufSize);

		switch(level)
		{
		case _LOG_DEBUG_MORE_BIT:
			offset = sprintf(buf, "%5u.%03u %6s - %15s:%-4d {%12s} - %32s - ",
					now / 1000, now % 1000,
					"|DEBM|",
					_m2m_log_get_file_title(file), line,
					m2m_os_get_current_task_name(taskName),
					function);
			break;
		case _LOG_DEBUG_BIT:
			offset = sprintf(buf, "%5u.%03u %6s - %15s:%-4d {%12s} - %32s - ",
					now / 1000, now % 1000,
					"|DEBG|",
					_m2m_log_get_file_title(file), line,
					m2m_os_get_current_task_name(taskName),
					function);
			break;
		case _LOG_VERB_BIT:
			offset = sprintf(buf, "%5u.%03u %6s - %15s:%-4d {%12s} - %32s - ",
					now / 1000, now % 1000,
					"|VERB|",
					_m2m_log_get_file_title(file), line,
					m2m_os_get_current_task_name(taskName),
					function);
			break;

		case _LOG_INFO_BIT:
			offset = 0;
			break;

		case _LOG_WARN_BIT:
			offset = sprintf(buf, "%5u.%03u %6s - %15s:%-4d {%12s} - %32s - ",
					now / 1000, now % 1000,
					"|WARN|",
					_m2m_log_get_file_title(file), line,
					m2m_os_get_current_task_name(taskName),
					function);
			break;

		case _LOG_ERROR_BIT:
			offset = sprintf(buf, "%5u.%03u %6s - %15s:%-4d {%12s} - %32s - ",
					now / 1000, now % 1000,
					"|ERR |",
					_m2m_log_get_file_title(file), line,
					m2m_os_get_current_task_name(taskName),
					function);
			break;

		default:
			break;
		}

		va_start(arg, fmt);
		vsnprintf(buf + offset, bufSize-offset, fmt, arg);
		va_end(arg);

		/* Print the message on the selected output stream */
		sent = _m2m_log_base_function(buf, NULL);

		m2mb_os_free(buf);
		buf = NULL;
	}

	return sent;

}



//##################################################################################################################################
/**
 *  \brief Print directly on the main UART
 *
 *	\param [in] message: the string to print
 *  \return sent bytes
 *
 */
//##################################################################################################################################
INT32 _m2m_log_print_to_UART(const char *message)
{
	INT32	sent = 0;
	M2MB_OS_SEM_ATTR_HANDLE semAttrHandle;


	if (NULL == USIF0SemHandle)
	{
		m2mb_os_sem_setAttrItem( &semAttrHandle, CMDS_ARGS( M2MB_OS_SEM_SEL_CMD_CREATE_ATTR,  NULL,M2MB_OS_SEM_SEL_CMD_COUNT, 1 /*CS*/, M2MB_OS_SEM_SEL_CMD_TYPE, M2MB_OS_SEM_GEN,M2MB_OS_SEM_SEL_CMD_NAME, "MainUartSem"));
		m2mb_os_sem_init( &USIF0SemHandle, &semAttrHandle );
	}

	m2mb_os_sem_get(USIF0SemHandle, M2MB_OS_WAIT_FOREVER );


	/* Get a UART handle first */
	if(g_UART_fd == -1)
	{
		g_UART_fd = m2mb_uart_open( "/dev/tty0", 0 );
	}

	if ( -1 != g_UART_fd)
	{
		sent = m2mb_uart_write(g_UART_fd, (char*) message, strlen(message));

		//	m2mb_uart_close(g_UART_fd);
	}

	m2mb_os_sem_put(USIF0SemHandle);
	return sent;
}


//##################################################################################################################################
/**
 *  \brief Print directly on the main UART
 *
 *	\param [in] message: the string to print
 *  \return sent bytes
 *
 */
//##################################################################################################################################
INT32 _m2m_log_print_to_AUX_UART(const char *message)
{
	INT32	sent = 0;
	M2MB_OS_SEM_ATTR_HANDLE semAttrHandle;




	if (NULL == USIF1SemHandle)
	{
		m2mb_os_sem_setAttrItem( &semAttrHandle, CMDS_ARGS( M2MB_OS_SEM_SEL_CMD_CREATE_ATTR,  NULL,M2MB_OS_SEM_SEL_CMD_COUNT, 1 /*CS*/, M2MB_OS_SEM_SEL_CMD_TYPE, M2MB_OS_SEM_GEN,M2MB_OS_SEM_SEL_CMD_NAME, "MainUartSem"));
		m2mb_os_sem_init( &USIF1SemHandle, &semAttrHandle );
	}

	m2mb_os_sem_get(USIF1SemHandle, M2MB_OS_WAIT_FOREVER );


	/* Get a UART handle first */
	if(g_AUX_fd == -1)
	{
		g_AUX_fd = m2mb_uart_open( "/dev/tty1", 0 );
	}

	if ( -1 != g_AUX_fd)
	{
		sent = m2mb_uart_write(g_AUX_fd, (char*) message, strlen(message));

		//m2mb_uart_close(g_AUX_fd);
	}

	m2mb_os_sem_put(USIF1SemHandle);
	return sent;
}




//##################################################################################################################################
/**
 *  \brief Prints as _m2m_log_printToUart  but using a specified USB channel
 *
 *  \param [in] path: 	  USB resource path where to print (e.g. /dev/USB0
 *  \param [in] message : Message to print
 *  \return sent bytes, negative in case of error
 *
 *  \details Using channel:USB_CH_DEFAULT uses channel assigned to instance USER_USB_INSTANCE_0
 */
//##################################################################################################################################
INT32  _m2m_log_print_to_USB (const CHAR *path, const char *message )
{
	INT32 ch;
	INT32 result;
	INT32 sent = 0;

	M2MB_OS_SEM_ATTR_HANDLE semAttrHandle;


#if 0
	if(! m2m_hw_usb_cable_check()) /*If usb cable is not plugged, simply exit.*/
	{
		return USB_CABLE_NOT_PLUG;
	}
	m2m_hw_usb_set_open_blocking(0);
#endif

	//get the requested channel from the path, so the related semaphore can be retrieved
	result = sscanf(path, "/dev/USB%d", &ch);
	if(!result)
	{
		return CANNOT_OPEN_USB_CH;
	}



	if (USBSemHandle[ch] == NULL)
	{
		m2mb_os_sem_setAttrItem( &semAttrHandle, CMDS_ARGS( M2MB_OS_SEM_SEL_CMD_CREATE_ATTR,  NULL,M2MB_OS_SEM_SEL_CMD_COUNT, 1 /*CS*/, M2MB_OS_SEM_SEL_CMD_TYPE, M2MB_OS_SEM_GEN,M2MB_OS_SEM_SEL_CMD_NAME, "UsbSem"));
		m2mb_os_sem_init( &USBSemHandle[ch], &semAttrHandle );
	}

	m2mb_os_sem_get(USBSemHandle[ch], M2MB_OS_WAIT_FOREVER );


	/* Get a USB handle first */
	if(g_USBfd == -1)
	{
		g_USBfd = m2mb_usb_open(path, 0);
	}

	if ( g_USBfd == -1 )
	{
		//destroy lock related to USB that can not be opened
		m2mb_os_sem_deinit(USBSemHandle[ch]);
		//free for later use, if any
		USBSemHandle[ch] = NULL;

		return CANNOT_OPEN_USB_CH;
	}


	sent = m2mb_usb_write ( g_USBfd, (const void*) message, strlen(message));

	/* in case of concurrency using m2m_hw_usb... comment the next API to avoid the closing */
	//(void)m2mb_usb_close(g_USBfd);

	m2mb_os_sem_put(USBSemHandle[ch]);

	return sent;
}


UINT32 _m2m_log_get_Uptime(void)
{

	UINT32 sysTicks = m2mb_os_getSysTicks();

	FLOAT32 ms_per_tick = m2mb_os_getSysTickDuration_ms();

	return (UINT32) (sysTicks * ms_per_tick); //milliseconds
}





