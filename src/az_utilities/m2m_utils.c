/*
 * m2m_utils.c
 *
 *  Created on: 06 apr 2018
 *      Author: AlessioQu
 */


#include "m2mb_types.h"
#include "m2mb_os_types.h"
#include "m2mb_os_api.h"

#include "m2m_utils.h"

void sleep_ms(UINT32 ms)
{
	m2mb_os_taskSleep( M2MB_OS_MS2TICKS(ms) );
}


