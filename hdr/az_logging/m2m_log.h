/*
 * m2m_log.h
 *
 *  Created on: 13 gen 2017
 *      Author: FabioPi
 */

#ifndef HDR_M2M_LOG_H_
#define HDR_M2M_LOG_H_


#include "m2mb_types.h"

/* This file provides logging utilities definitions. */


#define M2M_LOG_MASK_LEN 2 //2 bytes

/* Log level enumeration */
typedef enum
{
	_LOG_NONE_BIT 		= 0x0000,   /* Do not print anything */
	_LOG_ERROR_BIT 		= 0x0001,  	/* like VERB, but adds an "ERROR" at the beginning of the message */
	_LOG_WARN_BIT		= 0x0002,   /* like VERB, but adds a "WARNING" at the beginning of the message. */
	_LOG_INFO_BIT		= 0x0004, 	/* print the message only, without any additional info */
	_LOG_VERB_BIT		= 0x0008,	/* print source-file name, line of the instruction and task ID*/
	_LOG_DEBUG_BIT		= 0x0010,	/* Prints most of the messages, adds a "DEBUG" at the beginning of the message*/
	_LOG_DEBUG_MORE_BIT = 0x0020, 	/* Prints every message, adds a "DEBUG" at the beginning of the message*/

	_LOG_MAX_BIT   		= 0xFFFF

} M2M_LOG_LEVEL_E;


#define LOG_NONE_MASK 			_LOG_NONE_BIT
#define LOG_ERROR_MASK 			_LOG_ERROR_BIT
#define LOG_WARNING_MASK 		_LOG_ERROR_BIT 		| _LOG_WARN_BIT
#define LOG_INFO_MASK 			LOG_WARNING_MASK 	| _LOG_INFO_BIT  //Common log level
#define LOG_VERB_MASK 			LOG_INFO_MASK 		| _LOG_VERB_BIT  //verbose log level
#define LOG_DEBUG_MASK 			LOG_VERB_MASK 		| _LOG_DEBUG_BIT  //debug log level
#define LOG_DEBUG_MORE_MASK 	LOG_DEBUG_MASK 		| _LOG_DEBUG_MORE_BIT  //trace log level




/*Log errors*/
typedef enum
{
	BUF_ALLOC_ERROR = -100,
	NOT_INIT_ERROR,
	USB_CABLE_NOT_PLUG,
	NO_MORE_USB_INSTANCE,
	CANNOT_OPEN_USB_CH,

	MAX_ERROR_LEN
} _T_M2M_LOG_ERRORS;


typedef INT32 (*_m2m_log_function)(const char *, void*);



typedef enum
{
	_LOG_UART_MAIN, /* not available yet */
	_LOG_UART_AUX, 	/* AT#PORTCFG=0 	 */
	_LOG_USB0,		/* not available yet */
	_LOG_USB1, 		/* AT#PORTCFG=3 	 */

	_LOG_MAX
} M2M_LOG_HANDLE_T;


typedef struct
{
	UINT32 				logMask;
	M2M_LOG_HANDLE_T 	channel;
}M2M_LOG_CFG;


typedef struct
{
	UINT32 				gLog_Mask;
	M2M_LOG_HANDLE_T 	gLog_Channel;
	INT8				isInit;
} _M2M_LOG_STRUCT;


/* PUBLIC functions*/

void m2m_log_init(M2M_LOG_CFG *log_cfg);
INT32 m2m_log_deinit();
void m2m_log_set_log_mask(UINT32 log_mask);
UINT32 m2m_log_get_log_mask(void);
UINT32 * m2m_get_log_mask_pointer(void);


/* INTERNAL functions*/

UINT32 _m2m_log_get_Uptime(void);

INT32 _m2m_log_base_function(const char *msg, void *par1);

/* Message formatting function. It will print on UART or USB according with the defined output stream */
/* According to log level, prints additional information */
INT32 _m2m_log_formatted ( M2M_LOG_LEVEL_E level, const char *function, const char *file, int line, const CHAR *fmt, ... );


/* Returns the file name from the complete path */
const char* _m2m_log_get_file_title(const char *path);

/* Outputs the given data to the UART directly. */
INT32 _m2m_log_print_to_UART(const char *message);

INT32 _m2m_log_print_to_AUX_UART(const char *message);

/* Outputs the given data to the specified USB channel  directly. */
INT32 _m2m_log_print_to_USB(const CHAR*, const char *message);


/* MACROS */


#define M2M_LOG_ERROR(a...) 		_m2m_log_formatted(_LOG_ERROR_BIT,	__FUNCTION__, __FILE__, __LINE__, a)
#define M2M_LOG_WARN(a...)  		_m2m_log_formatted(_LOG_WARN_BIT, 	__FUNCTION__, __FILE__, __LINE__, a)
#define M2M_LOG_INFO(a...)  		_m2m_log_formatted(_LOG_INFO_BIT, 	"", "", 0,  a)
#define M2M_LOG_VERB(a...)   		_m2m_log_formatted(_LOG_VERB_BIT, 	__FUNCTION__, __FILE__, __LINE__, a)
#define M2M_LOG_DEBUG(a...)   		_m2m_log_formatted(_LOG_DEBUG_BIT, 	__FUNCTION__, __FILE__, __LINE__, a)
#define M2M_LOG_DEBUG_MORE(a...)   	_m2m_log_formatted(_LOG_DEBUG_MORE_BIT, __FUNCTION__, __FILE__, __LINE__, a)


#define M2M_SET_LOG_LEVEL_MASK(m, L) 	(m |= (1 << L))
#define M2M_UNSET_LOG_LEVEL_MASK(m, L) 	(m &= (~(1<< L)))
#define M2M_IS_LOG_LEVEL_SET(m, L) 		(m & L) ==  L

#endif /* HDR_M2M_LOG_H_ */
