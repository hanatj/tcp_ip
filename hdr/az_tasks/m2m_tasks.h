/*
 * m2m_tasks.h
 *
 *  Created on: 06 apr 2018
 *      Author: AlessioQu
 */

#ifndef HDR_M2M_TASKS_H_
#define HDR_M2M_TASKS_H_


typedef INT32 (*USER_TASK_CB)(INT32, INT32, INT32);


#define MIN_STACK_SIZE 1024
#define MAX_STACK_SIZE 32768

#define MIN_Q_SIZE 1
#define MAX_Q_SIZE 100


typedef enum
{
	M2M_TASK_OK = 1,

	M2M_TASK_NOTINIT_ERR = -1,
	M2M_TASK_WRONG_PRIO_ERR = -2,
	M2M_TASK_STACK_SIZE_ERR = -3,  //stack size out of bounds
	M2M_TASK_MSG_Q_SIZE_ERR = -4,
	M2M_TASK_NO_FREE_SLOTS_ERR = -5,
	M2M_TASK_ALLOC_ERR = -6,
	M2M_TASK_Q_ATTRIB_SET_ERR = -7,
	M2M_TASK_Q_INIT_ERR = -8,
	M2M_TASK_ATTRIB_SET_ERR = -9,
	M2M_TASK_CREATE_ERR = -10,

	M2M_TASK_INVALID_ID_ERR = -20,
	M2M_TASK_ID_NOT_DEFINED_ERR = -21,
	M2M_TASK_MSG_SEND_ERR = -22


} M2M_TASK_ERR_E;


typedef enum
{
	M2M_TASKS_STACK_S = 2048,    	/* 2K */
	M2M_TASKS_STACK_M = 4096,    	/* 4K */
	M2M_TASKS_STACK_L = 8192,    	/* 8K */
	M2M_TASKS_STACK_XL = 16384,  	/* 16K */

	M2M_OS_TASK_STACK_LIMIT = 32768 /* 32K */

}M2M_TASKS_STACK_SIZE;

typedef enum
{
	 M2M_TASKS_MBOX_S = 10,
	 M2M_TASKS_MBOX_M = 50,
	 M2M_TASKS_MBOX_L = 100,

	 M2M_TASKS_MBOX_LIMIT = 100

}M2M_TASKS_MBOX_SIZE;

#define M2M_OS_TASK_PRIORITY_MAX   1
#define M2M_OS_TASK_PRIORITY_MIN  32



#define _T_MAX_TASKS 32
#define _T_QUEUE_MSG_SIZE 3           // struct (type, param1, param2)
#define _T_TASK_NAME_SIZE 64


typedef struct _TASK_MESSAGE
{
   INT32  type;
   INT32  param1;
   INT32  param2;
} TASK_MESSAGE;


typedef struct
{
	M2MB_OS_TASK_HANDLE   Task_H;
	M2MB_OS_Q_HANDLE      Task_Queue_H;
	USER_TASK_CB		  Task_UserCB;
	UINT8 				  SlotInUse;
	char 				 *Task_NameBuf;
	UINT8 				 *Task_QueueArea; //[ 50 * 3 *4 ]; // 50 messages, each struct element is word32

} TASK_SLOT_STRUCT;

typedef struct
{
	TASK_SLOT_STRUCT task_slots[_T_MAX_TASKS];
	M2MB_OS_TASK_HANDLE M2MMain_Handle;
	INT8 isInit;
} _S_TASK_PARAMS;



/*!
  @brief
    m2m_task_first_init initializes the parameters needed to use tasks.

  @return
    returns success

  @b
    m2m_task_first_init();
*/
INT32 m2m_task_first_init();



/*!
  @brief
    m2m_create_task creates a new user task.

  @details
  	m2m_create_task creates a new user task with the provided stack size, priority, name and message queue size.
  	Calling m2m_task_first_init() is mandatory before using m2m_create_task.

  @param[in] *task_name
    first parameter is name that will be applied to new task. If NULL, a default name "Task<ID_number>" will be given,
    where <ID_number> is the newly created task id number. Max name len is 64 characters, if longer it will be truncated.
  @param[in] stack_size
    second parameter is the task stack size in bytes. Can be any value, but accepted range is 1024 - 32768
  @param[in] priority
  	third parameter is the task priority. The bigger the value, the less the priority of the task. Accepted range 1-32
  @param[in] msg_q_size
    fourth parameter is the task message queue slots number. Accepted range is 1-100
  @param[in] cb
    fifth parameter is the user callback that will be executed every time a message is received in task message queue.
    see USER_TASK_CB for the callback signature.
  @return
    returns task id number from 1 to 32 on success, -1 on failure.

  @b
    m2m_task_first_init();
*/
INT32 m2m_create_task( char *task_name, INT32 stack_size, INT32 priority, INT32 msg_q_size, USER_TASK_CB cb);

/*not available */
INT32 m2m_destroy_task(INT8 TaskProcID);

INT32 m2mb_os_send_message_to_task( INT8 TaskProcID, INT32 type, INT32 param1, INT32 param2 );

INT32 get_current_task_id(void);

char * m2mb_get_task_name(char *name);

#endif /* HDR_M2M_TASKS_H_ */
