/*
 * m2m_tcp_test.h
 *
 *  Created on: 09 apr 2018
 *      Author: AlessioQu
 */

#ifndef HDR_M2M_TCP_TEST_H_
#define HDR_M2M_TCP_TEST_H_


typedef enum {
	INIT=0,
	WAIT_FOR_REGISTRATION,
	REGISTERED,
	LAUNCH_TEST,
	SOCKET_CREATE,
	SOCKET_CONNECT,
	SOCKET_SEND,
	SOCKET_RECV,
	APPLICATION_EXIT
} APP_STATES;


INT32 M2M_msgTCPTask(INT32 type, INT32 param1, INT32 param2);
#endif /* HDR_M2M_TCP_TEST_H_ */
